# Active Disease Surveillance System

Once every week the Hospital Statistical Unit at Regional Hospital Limbe (RHL) collects data on disease occurrences and public health events. The data is recorded on a template, paper based format, from the departments of the hospital (maternity ward, TB ward, etc.). The data recorded is then sent into the District Health Information Software (DHIS 2), which combines data from all other health zones in Cameroon. RHL does not have a system of its own as a result they do not do simple analysis of the data or produce efficient well-customized reports. This project proposes a new system to be used by RHL which can be integrated with DHIS 2
called Active Disease Surveillance System.

Active Disease Surveillance System is a health information system involving the collection and analysis of data recorded in different departments or sections of the RHL reporting diseases, deaths, risk factors or any health-related events in a given period.

## Goals
The main goal is to design and develop a paperless digital system that will help RHL in the following tasks:
- Ease data collection
- Ease generation of reports
- Fast detection of epidemics
- Early identification of endemic and non-endemic diseases
- Ability to assess health status of a defined population
- Assist in identification of new and emerging diseases
- Availability of information for future planning and conducting research
- Verification of the absence of a specific disease


## Setup ACDIS
Create your environment
1. pipenv install Django
2. python manage.py createsuperuser
3. 

## Some other Sections Here


## Acknowledgments

This project use authentication app from https://github.com/wsvincent/djangox
