// ability to go back to previous page
$(document).ready(function(){
    $('.backLink').click(function(){
        parent.history.back();
        return false;
    });
});

// implementing datatables
$(document).ready(function() {
    $('#reportPreview').DataTable({
        "columnDefs": [ {
            "targets": [0],
            "searchable": true,
            } ]
    });
});

$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'print'
        ]
    } );
} );

$(document).ready(function() {
    $('#diseaseList').DataTable({
        "columnDefs": [ {
            "targets": [0,2],
            "searchable": false,
            } ]
    });
});


$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#incidentList tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#incidentList').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );

$(document).ready(function() {
    $('#reportList').DataTable( {
        
        initComplete: function () {
            this.api().columns([2, 3]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );


$('.from').datepicker();


$('.to').datepicker();

$(document).ready(function() {
    var table = $('#newReport').DataTable( {
        "scrollY":        "500px",
        "scrollCollapse": true,
        "paging":         false,
        "columnDefs": [ {
            "targets": [0],
            "searchable": false
            } ],
        "oLanguage": {
            "sSearch": "Quick Search:"
        }
    } );

    $('#newReport tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );
} );

$(document).ready(function() {
    var table = $('#predict').DataTable();
 
    $('button').click( function() {
        var data = table.$('input, select').serialize();
        alert(
            "The following data would have been submitted to the server: \n\n"+
            data.substr( 0, 120 )+'...'
        );
        return false;
    } );
} );