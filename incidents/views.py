from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy, reverse
from django.shortcuts import render, render_to_response

from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext

from tablib import Dataset
from .resources import IncidentResource

from . import models

from .utils import generate_template

import pandas as pd

from django.core.cache import cache
cache.clear()


class DiseaseCreateView(CreateView):
    model = models.Disease
    template_name = 'pages/disease_new.html'
    fields = ['name']
    #generate_template()

class DiseaseListView(ListView):
    model = models.Disease
    template_name = 'pages/disease_list.html'

class DiseaseUpdateView(UpdateView):
    fields = ['name']
    model = models.Disease
    template_name = 'pages/disease_edit.html'
    #generate_template()

class DiseaseDetailView(DetailView):
    model = models.Disease
    template_name = 'pages/disease_detail.html'

class DiseaseDeleteView(DeleteView):
    model = models.Disease
    template_name = 'pages/disease_delete.html'
    success_url = reverse_lazy('disease_list')
    #generate_template()

class IncidentListView(ListView):
    model = models.Incident
    template_name = 'pages/incident_list.html'

def upload(request):
    if request.method == 'POST':
        incident_resource = IncidentResource()
        dataset = Dataset()
        new_incidents = request.FILES['myfile']

        result = dataset.load(new_incidents.read().decode('utf-8'),format='csv')
        result = incident_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            incident_resource.import_data(dataset, dry_run=False)  # Actually import now
    return render(request, 'pages/report_upload.html')

def create_report(request):
    diseases = models.Disease.objects.all()
    return render_to_response('pages/report_new.html', {'range': range(1,53), 'diseases': diseases}, RequestContext(request))

def get_report_previews(request):
    if request.method == 'POST':
        df = pd.read_excel(request.FILES['myfile'])
        diseases = list(df['Disease'])
        cases = list(df['Cases'])
        deaths = list(df['Deaths'])
        report = zip(diseases, cases, deaths)
        return render_to_response('pages/report_preview.html', {'report': report}, RequestContext(request))

def get_reports_list(request):
    list = models.Incident.objects.values('year', 'week').distinct()
    return render_to_response('pages/report_list.html', {'list': list}, RequestContext(request))
    