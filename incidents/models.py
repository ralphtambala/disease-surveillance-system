from django.db import models
from django.urls import reverse

class Disease(models.Model):
    name = models.CharField(max_length=100, unique=True)
    # description = models.TextField(null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('disease_detail', args=[str(self.id)])

class Incident(models.Model):
    year = models.IntegerField()
    week = models.IntegerField()
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    cases = models.IntegerField()
    deaths = models.IntegerField()

    def __str__(self):
        return '%d-%d-%s' % (self.year, self.week, self.disease.name[:10])

    class Meta:
        ordering = ('year', 'week')