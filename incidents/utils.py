import pandas as pd
from . import models

def generate_template():
    diseases = models.Disease.objects.all()

    df = pd.DataFrame({'Data': diseases})

    # Convert the dataframe to an XlsxWriter Excel object.
    df.to_excel("static//docs//report_template.xlsx", sheet_name='Sheet1')