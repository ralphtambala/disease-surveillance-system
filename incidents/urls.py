from django.urls import path
from . import views

urlpatterns = [
    # routes for disease pages
    path('disease/list/', views.DiseaseListView.as_view(), name='disease_list'),
    path('<int:pk>/edit/', views.DiseaseUpdateView.as_view(), name='disease_edit'),
    path('disease/<int:pk>/', views.DiseaseDetailView.as_view(), name='disease_detail'),
    path('<int:pk>/delete/', views.DiseaseDeleteView.as_view(), name='disease_delete'),
    path('disease/new/', views.DiseaseCreateView.as_view(), name='disease_new'),

    # routes for incidents
    path('incident/list/', views.IncidentListView.as_view(), name='incident_list'),
    path('report/preview/', views.get_report_previews, name='report_preview'),
    path('report/list/', views.get_reports_list, name='report_list'),
    path('report/upload/', views.upload, name='report_upload'),
    path('report/new/', views.create_report, name='report_new'),
]