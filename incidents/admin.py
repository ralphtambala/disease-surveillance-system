from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import Disease, Incident

admin.site.register(Disease)
@admin.register(Incident)
class IncidentAdmin(ImportExportModelAdmin):
    pass